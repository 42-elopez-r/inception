#!/bin/bash

apt-get -y update
apt-get -y upgrade

apt-get -y install procps nginx openssl
apt-get -y autoremove

mv -v /nginx_config /etc/nginx/sites-available/default

mkdir -p /etc/nginx/ssl
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt << EOF
ES
Madrid
Madrid
Equestria
MLP
localhost
twilight@canterlot.sun
EOF

exec nginx -g 'daemon off;'
