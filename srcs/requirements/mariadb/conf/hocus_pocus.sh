#!/bin/bash

apt-get -y update
apt-get -y upgrade

apt-get -y install mariadb-server mariadb-client procps
apt-get -y autoremove

mkdir -p /run/mysqld
chown -R mysql:mysql /run/mysqld
mkdir -p /var/lib/mysql
chown -R mysql:mysql /var/lib/mysql

# So other hosts besides localhost can connect to the database
sed -i 's/bind-address            = 127.0.0.1//' /etc/mysql/mariadb.conf.d/50-server.cnf

mysqld --user=mysql &
mysqld_pid=$!
pgrep -x mysqld &> /dev/null || exit 1

sleep 3
mysql -u root << EOF
# mysql_secure_installation equivalent
UPDATE mysql.user SET Password=PASSWORD('$MYSQL_PASSWORD') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;

# Allow password login
USE mysql;
UPDATE user SET plugin='mysql_native_password' WHERE User='root';
GRANT ALL ON *.* to root@'%' IDENTIFIED BY '$MYSQL_PASSWORD' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF

kill $mysqld_pid
sleep 5
pgrep -x mysqld &> /dev/null && echo "$mysqld_pid chose death" && kill -9 $mysqld_pid && sleep 3

exec mysqld --user=mysql
