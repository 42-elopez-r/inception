#!/bin/bash

apt-get -y update
apt-get -y upgrade

apt-get -y install mariadb-client procps wget php-cgi php-common php-fpm php-pear php-mbstring php-zip php-net-socket php-gd php-xml-util php-gettext php-mysql php-bcmath
apt-get -y autoremove

mkdir -p /run/php

sed -i 's|listen = /run/php/php7.3-fpm.sock|listen = 9000|' /etc/php/*/fpm/pool.d/www.conf
if [ ! -f /var/www/html/index.php ]; then
	cd /tmp
	wget 'https://wordpress.org/wordpress-5.9.tar.gz'
	tar xf 'wordpress-5.9.tar.gz'
	rm 'wordpress-5.9.tar.gz'
	mv wordpress/* /var/www/html
	mv -v /wp-config.php /var/www/html

        mysql_ready=0
	while [ $mysql_ready -eq 0 ]; do
		mysql -u root -h mariadb -p$MYSQL_PASSWORD << EOF && mysql_ready=1
CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL ON wordpress.* TO '$WORDP_SQL_USER'@'%' IDENTIFIED BY '$WORDP_SQL_PASSWORD';
GRANT ALL ON wordpress.* TO 'root'@'%'; FLUSH PRIVILEGES;
USE wordpress;
SOURCE /wordpress.sql;
EOF
		sleep 3
	done
	rm /wordpress.sql
fi

echo 'Launching php-fpm...'
exec $(ls /usr/sbin/php-fpm* | tail -n 1) -F -R
