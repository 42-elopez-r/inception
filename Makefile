SHELL = /bin/sh

all: start

domain:
	if [ $(shell grep -c '127.0.0.1 elopez-r.42.fr' /etc/hosts) -eq 0 ]; then \
		echo '127.0.0.1 elopez-r.42.fr' | sudo tee -a /etc/hosts > /dev/null; \
	fi

start: domain
	docker-compose -f srcs/docker-compose.yml up

stop:
	docker-compose -f srcs/docker-compose.yml down

clean: stop
	sudo rm -rf ~/data
	docker image rm $(shell docker image ls -a | grep -v debian | tr -s ' ' | cut -d ' ' -f 3)

.PHONY: all start domain stop clean
